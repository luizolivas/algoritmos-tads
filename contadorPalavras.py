# Dada uma frase de entrada por um usuário, faça um programa que conte a ocorrência de
# cada palavra nesta frase. O programa não deve distinguir letras maiúsculas e minúsculas.

frase = str(input('Digite'))
divisao = frase.split(' ')
contagem = len(divisao)
dicionario = {}

for i in range(contagem):
    valor = divisao[i].lower()
    if valor in dicionario:
       dicionario[valor] = dicionario.get(valor) + 1    
    else:
        dicionario[valor] = 1
        
print (dicionario)