# Faça um programa que leia uma lista de 10 números inteiros dados pelo usuário. Seu
# programa deve somar todos os números da lista que sejam divisíveis por 3 ou por 5, e mostrar
# o resultado. Um número divisível por X é qualquer número menor cujo resto da divisão é igual a 0.

lista = []
contador = 0

for i in range(10):
    num = int(input('Digite'))
    if num % 3 == 0:
        lista.append(num)
    if num % 5 == 0:
        lista.append(num)
soma = sum(lista)
print (lista, '=', soma)