valor_imposto =[]
faixa_BC = []
Renda_Total = 0
imposto = 0
faixa1 = 1903.98
faixa2 = 2826.65
faixa3 = 3751.05
faixa4 = 4664.68
contador = 0

decisao = 's'
while decisao != 'n':
    base = float(input('Digite o seu salário: '))
    Renda_Total += base
    if base > faixa4:
        base_calculo = (base - faixa4)
        faixa_BC.append(base_calculo)
        imposto = ((base - faixa4) * 0.275)
        valor_imposto.append(imposto)
        base = faixa4

    if base > faixa3:
        base_calculo = (base - faixa3)
        faixa_BC.append(base_calculo)
        imposto = ((base - faixa3) * 0.225)
        valor_imposto.append(imposto)
        base = faixa3

    if base > faixa2:
        base_calculo = (base - faixa2)
        faixa_BC.append(base_calculo)    
        imposto = ((base - faixa2) * 0.15)
        valor_imposto.append(imposto)
        base = faixa2

    if base > faixa1:
        base_calculo = (base - faixa1)
        faixa_BC.append(base_calculo)
        imposto = ((base - faixa1) * 0.075)
        valor_imposto.append(imposto)
        base = faixa1

    if base > 0:
        base_calculo = (base - 0 )
        faixa_BC.append(base_calculo)
        imposto = ((base - 0) * 0)
        valor_imposto.append(imposto)
        base = 0

    contador += 1
    decisao = input('Deseja inserir mais algum salário? (s/n) ')

def Media_mensal (all_renda,divisor):

    media = all_renda / divisor
    
    return "%.2f" % media

total_ifpr = sum(valor_imposto)
def aliquota (ifpr_total,renda_total):
    calculo = ifpr_total / renda_total
    
    porcentagem = calculo * 100

    return "%.2f" % porcentagem

def irpf_medio(ifpr_total,contador):
    
    media = ifpr_total / contador

    return "%.2f" % media

print ('A Renda total é:',Renda_Total)
print ('A Renda média mensal é:',Media_mensal(Renda_Total,contador))
print ('O imposto total é:',sum(valor_imposto))
print ('O IRPF médio é:', irpf_medio(total_ifpr,contador))
print ('A aliquota é:', aliquota(total_ifpr,Renda_Total),'%')