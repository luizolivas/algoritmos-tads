# Implemente um programa no qual o usuário possa digitar uma lista de números inteiros. O
# programa deve mostrar os elementos desta lista na ordem inversa, porém sem os números
# ímpares.

decisao = 's'
lista = []
valor = 0
while decisao != 'n':
    valor = int(input('Digite um número'))
    decisao = input('Deseja continuar inserindo números? (s/n)')

    if valor % 2 !=1 :
        lista.append(valor)
        lista.reverse()

print (lista)