# Um palíndromo é uma palavra cujas letras formam a mesma pronúncia tanto quando lidas
# da esquerda para a direita quanto da direita para esquerda, como as palavras: arara, osso,
# saias, etc. Escreva um programa que receba uma string digitada por um usuário e que seja
# capaz de identificar um palíndromo nesta string.

frase = str(input('Digite a palavra').replace(" ", ""))
inverso = (frase[::-1])

if inverso == frase:
    print ('A palavra é um Palindromo: ', '\n',inverso , '\n',frase)
elif inverso != frase:
    print ('Não é um Palindromo.')