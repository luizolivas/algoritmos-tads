# Dado um número inteiro positivo N como entrada de um usuário, escreva um programa
# que mostre os primeiros N números da sequência de Fibonacci. (A sequência de Fibonacci é
# iniciada por 0 e 1, e o próximo número é sempre a soma dos dois últimos).

num_termos = int(input("Digite o número de termos"))
num1 = 0
num2 = 1
lista = []
contador = 2

while contador <= num_termos:
    fib = (num1 + num2)
    num2 = num1
    num1 = fib
    contador += 1
    lista.append(fib)
print ('O número de termos da sequência de Fibonacci é:',lista)