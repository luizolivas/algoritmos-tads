# 1 - Escreva um programa que mostre a soma de todos os divisores de um dado número X. Um
# divisor é qualquer número menor que X cujo resto da divisão é igual a 0.


lista = []
num = int(input("Digite um número: "))
for i in range(1, num):
    if num % i == 0:
        lista.append(i)
somaDivs = sum(lista)
print("Resultado: ", somaDivs)
print("Os divisores são: ", lista)