# Faça um programa que leia uma string de números como entrada de um usuário que
# representa uma matriz e diga qual dimensão dela (quantidade de linhas e colunas). As linhas
# da matriz são separadas por um caractere de ponto e vírgula ‘;’e as colunas são separadas por
# um espaço vazio ‘ ’.

matriz = str(input('Insira os valores da matriz: '))
numeros_linha = 1
numeros_coluna = 1
for i in range(0,len(matriz)):
    if matriz[i] == ';':
       numeros_linha = numeros_linha + 1
    if matriz[i] == ' ' and numeros_linha < 2:
        numeros_coluna = numeros_coluna + 1 
print ('resultado', numeros_linha,'X', numeros_coluna)